# Delivery dei servizi

Repository per versionare gli sviluppi dell'app no-code [Windmill](https://windmill.opencontent.it/) per il delivery dei servizi. Attualmente viene versionata solo l'interfaccia che permette il delivery di più servizi in più tenant tramite il suo json descrittivo.


## Json descrittivo

Gli applicativi Windmill sono esportabili e importabili tramite un json che ne descrive la struttura. Questo json è recuperabile andando nell'interfaccia di edit dell'app e cliccare l'icone del menù con i 3 puntini verticali (kebab menu) in alto a destra prima dell'icona "debug runs", cliccare sulla voce "Json" e copiarne il contenuto tramite l'apposito bottone in alto a destra.